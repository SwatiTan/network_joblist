package google.networkapp_retrofit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class JobAdapter extends BaseAdapter {

    private final Context context;
    private final List<JobDatum> myJobs;

    public JobAdapter(Context context, List<JobDatum> myJobs) {
        this.context = context;
        this.myJobs = myJobs;
    }

    @Override
    public int getCount() {
        return myJobs.size();
    }

    @Override
    public Object getItem(int position) {
        return myJobs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_job_row, parent, false);
        TextView jobID = (TextView) view.findViewById(R.id.jobID);
        TextView jobStatus = (TextView) view.findViewById(R.id.jobStatus);
        TextView jobDate = (TextView) view.findViewById(R.id.jobDate);
        TextView subcategoryName = (TextView) view.findViewById(R.id.subcategoryName);

        final JobDatum myJob = myJobs.get(position);

        jobID.setText(myJob.getJobID());
        jobStatus.setText(String.valueOf(myJob.getJobStatus()));
        jobDate.setText(String.valueOf(myJob.getJobDate()));
        subcategoryName.setText(String.valueOf(myJob.getSubcategoryName()));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JobDetailActivity.class);
                String message = myJob.getJobID();
                intent.putExtra("jobID", message);
                context.startActivity(intent);

            }
        });
        return view;
    }
}

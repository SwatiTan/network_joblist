package google.networkapp_retrofit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyJobs {

    private String status;
    private List<JobDatum> jobData = new ArrayList<JobDatum>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The jobData
     */
    public List<JobDatum> getJobData() {
        return jobData;
    }

    /**
     *
     * @param jobData
     * The jobData
     */
    public void setJobData(List<JobDatum> jobData) {
        this.jobData = jobData;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}


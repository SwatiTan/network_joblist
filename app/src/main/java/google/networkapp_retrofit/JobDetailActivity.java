package google.networkapp_retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class JobDetailActivity extends AppCompatActivity {
    JobDetailHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        String jobId = getIntent().getStringExtra("jobID");
        db = new JobDetailHelper(this);
        JobDatum job =db.readJob(jobId);
        TextView jobID = (TextView) findViewById(R.id.jobIDVal);
        TextView jobStatus = (TextView) findViewById(R.id.jobStatusVal);
        TextView jobDate = (TextView) findViewById(R.id.jobDateVal);
        TextView subcategoryName = (TextView) findViewById(R.id.subcategoryNameVal);
        TextView jobAddress = (TextView) findViewById(R.id.areaPinCodeVal);
        TextView areaPinCode = (TextView) findViewById(R.id.subcategoryNameVal);
        TextView totalCost = (TextView) findViewById(R.id.totalCostVal);
        TextView discountReceived = (TextView) findViewById(R.id.discountReceivedVal);
        TextView jobPaymentType = (TextView) findViewById(R.id.jobPaymentTypeVal);
        TextView jobTimeHours = (TextView) findViewById(R.id.jobTimeHoursVal);
        TextView workerName = (TextView) findViewById(R.id.workerNameVal);
        TextView workerMobileNumber = (TextView) findViewById(R.id.workerMobileNumberVal);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package google.networkapp_retrofit;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by swati on 08-09-2015.
 */
public class JobDatum{

private String jobID;
private String jobStatus;
private String jobDate;
private String subcategoryName;
private String jobAddress;
private String areaPinCode;
private String totalCost;
private String discountReceived;
private String jobPaymentType;
private String jobTimeHours;
private String workerName;
private String workerMobileNumber;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         *
         * @return
         * The jobID
         */
        public String getJobID() {
            return jobID;
        }

        /**
         *
         * @param jobID
         * The jobID
         */
        public void setJobID(String jobID) {
            this.jobID = jobID;
        }

        /**
         *
         * @return
         * The jobStatus
         */
        public String getJobStatus() {
            return jobStatus;
        }

        /**
         *
         * @param jobStatus
         * The jobStatus
         */
        public void setJobStatus(String jobStatus) {
            this.jobStatus = jobStatus;
        }

        /**
         *
         * @return
         * The jobDate
         */
        public String getJobDate() {
            return jobDate;
        }

        /**
         *
         * @param jobDate
         * The jobDate
         */
        public void setJobDate(String jobDate) {
            this.jobDate = jobDate;
        }

        /**
         *
         * @return
         * The subcategoryName
         */
        public String getSubcategoryName() {
            return subcategoryName;
        }

        /**
         *
         * @param subcategoryName
         * The subcategoryName
         */
        public void setSubcategoryName(String subcategoryName) {
            this.subcategoryName = subcategoryName;
        }

        /**
         *
         * @return
         * The jobAddress
         */
        public String getJobAddress() {
            return jobAddress;
        }

        /**
         *
         * @param jobAddress
         * The jobAddress
         */
        public void setJobAddress(String jobAddress) {
            this.jobAddress = jobAddress;
        }

        /**
         *
         * @return
         * The areaPinCode
         */
        public String getAreaPinCode() {
            return areaPinCode;
        }

        /**
         *
         * @param areaPinCode
         * The areaPinCode
         */
        public void setAreaPinCode(String areaPinCode) {
            this.areaPinCode = areaPinCode;
        }

        /**
         *
         * @return
         * The totalCost
         */
        public String getTotalCost() {
            return totalCost;
        }

        /**
         *
         * @param totalCost
         * The totalCost
         */
        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        /**
         *
         * @return
         * The discountReceived
         */
        public String getDiscountReceived() {
            return discountReceived;
        }

        /**
         *
         * @param discountReceived
         * The discountReceived
         */
        public void setDiscountReceived(String discountReceived) {
            this.discountReceived = discountReceived;
        }

        /**
         *
         * @return
         * The jobPaymentType
         */
        public String getJobPaymentType() {
            return jobPaymentType;
        }

        /**
         *
         * @param jobPaymentType
         * The jobPaymentType
         */
        public void setJobPaymentType(String jobPaymentType) {
            this.jobPaymentType = jobPaymentType;
        }

        /**
         *
         * @return
         * The jobTimeHours
         */
        public String getJobTimeHours() {
            return jobTimeHours;
        }

        /**
         *
         * @param jobTimeHours
         * The jobTimeHours
         */
        public void setJobTimeHours(String jobTimeHours) {
            this.jobTimeHours = jobTimeHours;
        }

        /**
         *
         * @return
         * The workerName
         */
        public String getWorkerName() {
            return workerName;
        }

        /**
         *
         * @param workerName
         * The workerName
         */
        public void setWorkerName(String workerName) {
            this.workerName = workerName;
        }

        /**
         *
         * @return
         * The workerMobileNumber
         */
        public String getWorkerMobileNumber() {
            return workerMobileNumber;
        }

        /**
         *
         * @param workerMobileNumber
         * The workerMobileNumber
         */
        public void setWorkerMobileNumber(String workerMobileNumber) {
            this.workerMobileNumber = workerMobileNumber;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

}
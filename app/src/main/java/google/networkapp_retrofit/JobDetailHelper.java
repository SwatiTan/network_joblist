package google.networkapp_retrofit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class JobDetailHelper {
    public static final String DATABASE_NAME = "myjobs.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "myjobs";
    public static final String COLUMN_JOBID = "jobID";
    public static final String COLUMN_JOBSTATUS = "jobStatus";
    public static final String COLUMN_JOBDATE = "jobDate";
    public static final String COLUMN_SUBCATEGORYNAME ="subcategoryName";
    public static final String COLUMN_JOBADDRESS= "jobAddress";
    public static final String COLUMN_AREAPINCODE = "areaPinCode";
    public static final String COLUMN_TOTALCOST = "totalCost";
    public static final String COLUMN_DISCOUNTRECEIVED = "discountReceived";
    public static final String COLUMN_JOBPAYMENTTYPE = "jobPaymentType";
    public static final String COLUMN_JOBTIMEHOURS = "jobTimeHours";
    public static final String COLUMN_WORKERNAME ="workerName";
    public static final String COLUMN_WORKERMOBILNUMBER="workerMobileNumber";
    private static final String TABLE_CREATION_QUERY = "create table " + TABLE_NAME + "(" +
            COLUMN_JOBID + " integer primary key autoincrement," +
            COLUMN_JOBSTATUS + " text," +
            COLUMN_JOBDATE + " text," +
            COLUMN_SUBCATEGORYNAME +  " text," +
            COLUMN_JOBADDRESS +  " text," +
            COLUMN_AREAPINCODE+  " text," +
            COLUMN_TOTALCOST +  " text," +
            COLUMN_DISCOUNTRECEIVED +  " text," +
            COLUMN_JOBPAYMENTTYPE +  " text," +
            COLUMN_JOBTIMEHOURS+  " text," +
            COLUMN_WORKERNAME+  " text," +
            COLUMN_WORKERMOBILNUMBER
            +" text);";

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private Context context;

    public JobDetailHelper(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public void open() {
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void addJob(JobDatum jobDatum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_JOBID, jobDatum.getJobID());
        contentValues.put(COLUMN_JOBSTATUS, jobDatum.getJobStatus());
        contentValues.put(COLUMN_JOBDATE, jobDatum.getJobDate());
        contentValues.put(COLUMN_SUBCATEGORYNAME, jobDatum.getSubcategoryName());
        contentValues.put(COLUMN_JOBADDRESS, jobDatum.getJobAddress());
        contentValues.put(COLUMN_AREAPINCODE, jobDatum.getAreaPinCode());
        contentValues.put(COLUMN_TOTALCOST, jobDatum.getTotalCost());
        contentValues.put(COLUMN_DISCOUNTRECEIVED, jobDatum.getDiscountReceived());
        contentValues.put(COLUMN_JOBPAYMENTTYPE, jobDatum.getJobPaymentType());
        contentValues.put(COLUMN_JOBTIMEHOURS, jobDatum.getJobTimeHours());
        contentValues.put(COLUMN_WORKERNAME, jobDatum.getWorkerName());
        contentValues.put(COLUMN_WORKERMOBILNUMBER, jobDatum.getWorkerMobileNumber());
        long rowId = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        if (rowId != -1) {
            Toast.makeText(context, "Insert Success " + rowId, Toast.LENGTH_SHORT).show();
        }
    }

    public JobDatum  readJob(String jobId) {
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, null,
                COLUMN_JOBID + "=? " , new String[]{jobId}, null, null, null);

        JobDatum jobDatum = new JobDatum ();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                jobDatum.setJobID(cursor.getString(cursor.getColumnIndex(COLUMN_JOBID)));
                jobDatum.setJobStatus(cursor.getString(cursor.getColumnIndex(COLUMN_JOBSTATUS)));
                jobDatum.setJobDate(cursor.getString(cursor.getColumnIndex(COLUMN_JOBDATE)));
                jobDatum.setSubcategoryName(cursor.getString(cursor.getColumnIndex(COLUMN_SUBCATEGORYNAME)));
                jobDatum.setJobAddress(cursor.getString(cursor.getColumnIndex(COLUMN_JOBADDRESS)));
                jobDatum.setAreaPinCode(cursor.getString(cursor.getColumnIndex(COLUMN_AREAPINCODE)));
                jobDatum.setTotalCost(cursor.getString(cursor.getColumnIndex(COLUMN_TOTALCOST)));
                jobDatum.setDiscountReceived(cursor.getString(cursor.getColumnIndex(COLUMN_DISCOUNTRECEIVED)));
                jobDatum.setJobPaymentType(cursor.getString(cursor.getColumnIndex(COLUMN_JOBPAYMENTTYPE)));
                jobDatum.setJobTimeHours(cursor.getString(cursor.getColumnIndex(COLUMN_JOBTIMEHOURS)));
                jobDatum.setWorkerName(cursor.getString(cursor.getColumnIndex(COLUMN_WORKERNAME)));
                jobDatum.setWorkerMobileNumber(cursor.getString(cursor.getColumnIndex(COLUMN_WORKERMOBILNUMBER)));

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        sqLiteDatabase.close();
        return jobDatum;
    }

    public List<JobDatum> readJobForListView() {
        ArrayList<JobDatum> jobList
                = new ArrayList<JobDatum>();
        Cursor cursor =sqLiteDatabase.
                query(TABLE_NAME, null, null, null, null, null, null);
        if (cursor.moveToFirst())
        {
            do {
                JobDatum cr1 = new JobDatum();
                cr1.setJobID(cursor.getString(0));
                cr1.setJobStatus(cursor.getString(1));
                cr1.setJobDate(cursor.getString(2));
                cr1.setSubcategoryName(cursor.getString(3));
                cr1.setJobAddress(cursor.getString(4));
                cr1.setAreaPinCode(cursor.getString(5));
                cr1.setTotalCost(cursor.getString(6));
                cr1.setDiscountReceived(cursor.getString(7));
                cr1.setJobPaymentType(cursor.getString(8));
                cr1.setJobTimeHours(cursor.getString(9));
                cr1.setWorkerName(cursor.getString(10));
                cr1.setWorkerMobileNumber(cursor.getString(11));
                //Log.w("Rec in db", 	cr1.name );
                jobList.add(cr1);
                cr1 = null;
            } while (cursor.moveToNext());
        }
        sqLiteDatabase.close();
        return jobList;

            }

    public void close() {
        sqLiteDatabase.close();
    }


    class DBHelper extends SQLiteOpenHelper {

        // creating database
        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // creating tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CREATION_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            //Log.w("tables dropped", "Onupgrade");
            //Log.w("tables dropped2", "Onupgrade");
            onCreate(db);
        }
    }
}

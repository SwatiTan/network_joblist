package google.networkapp_retrofit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button fetchJobsButton;
    private ListView jobListView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fetchJobsButton = (Button) findViewById(R.id.fetchJobsButton);
        jobListView = (ListView) findViewById(R.id.jobListView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        fetchJobsButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        progressBar.setVisibility(View.VISIBLE);
        jobListView.setVisibility(View.GONE);

        RestAdapter restAdapter = new RestAdapter.Builder()
               // .setEndpoint("http://www.json-generator.com/"
               .setEndpoint("http://52.69.49.40/")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        JobListService service = restAdapter.create(JobListService.class);
        service.fetchJobs(new Callback<MyJobs>() {
            @Override
            public void success(MyJobs myJobs, Response response) {
                progressBar.setVisibility(View.GONE);
                jobListView.setVisibility(View.VISIBLE);
                JobAdapter myJobAdapter = new JobAdapter(MainActivity.this,
                        myJobs.getJobData());
                jobListView.setAdapter(myJobAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, error.getResponse().getReason(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }


}

package google.networkapp_retrofit;

import retrofit.Callback;
import retrofit.http.POST;

public interface JobListService {
  //  @GET("/api/json/get/cfdDIwoXoy?indent=2")
  @POST("challenge/fetchJobs.php?mobileNumber=9076323546")
    void fetchJobs(Callback<MyJobs> jobsCallback);
}
